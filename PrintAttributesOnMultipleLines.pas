procedure DBMemo1OnGetMemo(Lines: TStrings);
const
  Delimiter = '|';
var
  I: Integer;
  AttribString: String;
  TempString: String;
begin
  AttribString := ItemVariables['AttributeString'];
  TempString := '';

  FOR I := 1 TO Length(AttribString) DO
  Begin
    If COPY(AttribString,I,1) = Delimiter Then
    Begin
      Lines.Add(TempString);
      TempString := '';
    End
    Else
      TempString := TempString + Copy(AttribString,I,1);
  End;

  If TempString <> '' Then
    Lines.Add(TempString);
end;